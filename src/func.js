const getSum = (str1, str2) => {
    if (typeof str1 !== "string" || typeof str2 !== "string") return false;

    const addends = [str1, str2]
        .map(str => str.split("").map(s => Number(s)))
        .map(arr => (arr.length === 0 ? [0] : arr));
    if (addends.some(arr => arr.some(digit => isNaN(digit)))) return false;

    let sum = [];
    let additive = 0;
    for (let i = Math.max(addends[0].length, addends[1].length); i > -1; i--) {
        let digitSum =
            addends.map(arr => arr[i] || 0).reduce((prev, next) => prev + next) + additive;

        if (digitSum % 10 !== 0) sum.unshift(digitSum % 10);
        additive = Math.floor(digitSum / 10);
    }

    return sum.join("");
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
    const postCount = listOfPosts.filter(post => post.author === authorName).length;
    const commentCount = listOfPosts
        .flatMap(post => post.comments || [])
        .filter(comment => comment.author === authorName).length;

    return `Post:${postCount},comments:${commentCount}`;
};

const tickets = people => {
    people = people.map(val => Number(val));

    const bills = { 100: 0, 50: 0, 25: 0 };
    for (const value of people) {
        let change = value - 25;
        for (const bill of [50, 25])
            while (change >= bill && bills[bill] > 0) {
                change -= bill;
                bills[bill]--;
            }

        if (change > 0) return "NO";
        bills[value]++;
    }
    return "YES";
};

module.exports = { getSum, getQuantityPostsByAuthor, tickets };
